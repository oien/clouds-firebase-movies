import type { Unsubscribe } from "@firebase/util";

export interface IMovieRaw {
  id: number;
  genre: string;
  year: number | string;
  title: string | number;
}
export interface IMovie extends IMovieRaw {
  year: string;
  title: string;
}

export interface IListeners {
  [key: string]: Unsubscribe;
}

export interface IWishlist {
  [key: string]: number;
}
