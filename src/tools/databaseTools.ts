import { getAuth, User } from "@firebase/auth";

export function getCurrentUser(): Promise<User | null> {
  const auth = getAuth();
  return new Promise((resolve, reject) => {
    if (auth.currentUser) {
      resolve(auth.currentUser);
    }
    const unsubscribe = auth.onAuthStateChanged((user) => {
      unsubscribe();
      resolve(user);
    }, reject);
  });
}

export function getUsername(user: User | null) {
  if (!user) return "";
  return user.displayName!.toLowerCase().split(" ").join("_");
}
