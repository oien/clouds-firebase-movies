import { computed, ref, Ref, watch } from "@vue/runtime-dom";
import { IMovie } from "../types";

export default function useMatchedMovies(movies: Ref<IMovie[]>) {
  const searchQuery = ref("");
  const limit = ref(50);

  const matchPredicate = ({ genre, title, year }: IMovie) =>
    genre.toLowerCase().indexOf(searchQuery.value) > -1 ||
    title.toLowerCase().indexOf(searchQuery.value) > -1 ||
    year.toLowerCase().indexOf(searchQuery.value) > -1;

  const loadMore = () => {
    limit.value += 50;
  };

  const hasMore = computed(() => movies.value && limit.value < movies.value.length);

  const moviesMatchingSearchQuery = computed(() => {
    if (searchQuery.value.length === 0) return movies.value.slice(0, limit.value);
    return movies.value.filter(matchPredicate).slice(0, limit.value);
  });

  return { searchQuery, hasMore, loadMore, moviesMatchingSearchQuery };
}
