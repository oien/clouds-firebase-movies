import { onSnapshot, getFirestore, doc, setDoc } from "firebase/firestore";
import { ref, onMounted, onBeforeUnmount, computed } from "@vue/runtime-dom";
import { getUsername } from "../tools/databaseTools";
import { IListeners } from "../types";
import { getAuth } from "@firebase/auth";
import unsubscibeListeners from "../tools/unsubscribeListeners";

interface IWishlistUser {
  wishlist: number[];
}

export default function useWishlist() {
  const wishlist = ref<number[]>([]);

  const username = getUsername(getAuth().currentUser);
  const db = getFirestore();
  const wishlistRef = doc(db, "users", username);
  const listeners: IListeners = { wishlist: () => {} };

  const fetchWishlist = () => {
    listeners.wishlist = onSnapshot(wishlistRef, (querySnapshot) => {
      if (querySnapshot.exists()) {
        wishlist.value = (querySnapshot.data() as IWishlistUser).wishlist;
      } else {
        setDoc(wishlistRef, { wishlist: [] });
      }
    });
  };

  const isEmpty = computed(() => wishlist.value.length === 0);

  const addWish = (id: number) => {
    setDoc(wishlistRef, { wishlist: [...wishlist.value, id] });
  };

  const deleteWish = (deleteId: number) => {
    if (wishlist.value.length === 0) {
      return console.log("No wishlist, shoult not be able to delete anything");
    }
    const newList = wishlist.value.filter((id) => id !== deleteId);
    setDoc(wishlistRef, { wishlist: newList });
  };

  onMounted(fetchWishlist);
  onBeforeUnmount(() => unsubscibeListeners(listeners));

  return { wishlist, isEmpty, addWish, deleteWish };
}
