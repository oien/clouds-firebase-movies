import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import { FirebaseOptions, initializeApp } from "firebase/app";
import "./index.css";
import FontAwesomeIcon from "./fontawesome-icons";
import { key, store } from "./store";

const firebaseConfig: FirebaseOptions = {
  apiKey: import.meta.env.VITE_APIKEY as string,
  authDomain: import.meta.env.VITE_AUTHDOMAIN as string,
  projectId: import.meta.env.VITE_PROJECTID as string,
  storageBucket: import.meta.env.VITE_STORAGEBUCKET as string,
  messagingSenderId: import.meta.env.VITE_MESSAGINGSENDERID as string,
  appId: import.meta.env.VITE_APPID as string,
  measurementId: import.meta.env.VITE_MEASUREMENTID as string,
  databaseURL: import.meta.env.VITE_DATABASEURL as string,
};

// Initialize Firebase
const firebaseApp = initializeApp(firebaseConfig);
// const analytics = getAnalytics(firebaseApp);

const app = createApp(App)
  .component("font-awesome-icon", FontAwesomeIcon)
  .use(router)
  .use(store, key);
app.mount("#app");
