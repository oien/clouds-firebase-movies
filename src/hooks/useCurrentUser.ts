import { computed, onBeforeUnmount, ref } from "vue";
import { getAuth, User } from "firebase/auth";
import { useRouter } from "vue-router";

export default function useCurrentUser() {
  const auth = getAuth();
  const router = useRouter();
  const currentUser = ref<User | null>(null);

  const authListener = auth.onAuthStateChanged(function (user) {
    currentUser.value = user;
  });

  const loginName = computed(() => {
    if (currentUser.value == null) return "";
    return currentUser.value?.displayName || "Unknown";
  });

  const signOut = () => {
    auth.signOut();
    router.push("/");
  };

  onBeforeUnmount(() => authListener());

  return { currentUser, signOut, loginName };
}
