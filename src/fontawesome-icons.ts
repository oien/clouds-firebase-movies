import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faHeart, faHeartBroken, faSpinner, faUser } from "@fortawesome/free-solid-svg-icons";

library.add(faUser, faSpinner, faHeart, faHeartBroken);
// library.add(faSpinner)

export default FontAwesomeIcon;
