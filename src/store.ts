import { get, getDatabase, ref } from "firebase/database";
import { InjectionKey } from "vue";
import { createStore, Store, useStore as baseUseStore } from "vuex";
import { IMovie, IMovieRaw } from "./types";

interface IGlobalState {
  movies: IMovie[];
  loading: boolean;
  error: boolean;
  hasLoaded: boolean;
}

export const LOAD_MOVIES = "LOAD_MOVIES";
export const SET_LOADING = "SET_LOADING";
export const SET_ERROR = "SET_ERROR";

export const FETCH_MOVIES = "FETCH_MOVIES";

export const store = createStore<IGlobalState>({
  state: {
    movies: [],
    loading: true,
    error: false,
    hasLoaded: false,
  },
  mutations: {
    [LOAD_MOVIES](state, movies: IMovie[]) {
      state.movies = movies;
      state.hasLoaded = true;
    },
    [SET_LOADING](state, isLoading: boolean) {
      state.loading = isLoading;
    },
    [SET_ERROR](state, error: boolean) {
      state.loading = error;
    },
  },
  actions: {
    async [FETCH_MOVIES]({ commit, state }) {
      if (state.hasLoaded) return;

      commit(SET_LOADING, true);
      commit(SET_ERROR, false);
      const db = getDatabase();
      const moviesRef = ref(db, "movies-list/");
      try {
        const moviesSnap = await get(moviesRef);
        const _movies: IMovie[] = [];
        moviesSnap.forEach((movieSnap) => {
          const movie = movieSnap.val() as IMovieRaw;
          _movies.push({
            ...movie,
            title: movie.title.toString(),
            year: movie.year.toString(),
          });
        });
        commit(LOAD_MOVIES, _movies);
        commit(SET_LOADING, false);
      } catch (err) {
        console.log("ERROR");
        commit(SET_LOADING, false);
        commit(SET_ERROR, true);
      }
    },
  },
});

// define injection key
export const key: InjectionKey<Store<IGlobalState>> = Symbol();

// typed useStore
export function useStore() {
  return baseUseStore(key);
}
