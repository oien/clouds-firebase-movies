import { IListeners } from "../types";

export default function unsubscibeListeners(listeners: IListeners) {
  Object.values(listeners).forEach((unsubFunc) => unsubFunc());
}
