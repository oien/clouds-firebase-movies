import { ref, watch, onBeforeUnmount, Ref, ComputedRef } from "@vue/runtime-dom";
import { IListeners, IMovie } from "../types";
import unsubscibeListeners from "../tools/unsubscribeListeners";

export default function useFilteredMovies(
  movies: ComputedRef<IMovie[]>,
  wishlist: Ref<number[]>,
  isWishlist: boolean
) {
  const wishlistFilteredMovies = ref<IMovie[]>([]);
  const listeners: IListeners = { wishlist: () => {} };

  const includesId = (id: number) => wishlist.value.includes(id);

  const excWishlist = ({ id }: IMovie) => !includesId(id);
  const incWishlist = ({ id }: IMovie) => includesId(id);

  const filterMovies = () => {
    if (!wishlist.value || wishlist.value.length === 0) {
      wishlistFilteredMovies.value = movies.value;
      return; 
    }
    wishlistFilteredMovies.value = movies.value.filter(isWishlist ? incWishlist : excWishlist);
  };

  // Shouldn't strictly be necessary for movies, but seems like there is something wrong with
  // my understanding of passing the ref: https://vuejs.org/v2/guide/reactivity.html#Change-Detection-Caveats
  watch([wishlist, movies], filterMovies);

  onBeforeUnmount(() => unsubscibeListeners(listeners));

  return { wishlistFilteredMovies };
}
