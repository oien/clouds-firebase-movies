import { createRouter, createWebHistory } from "vue-router";
import { getCurrentUser } from "../tools/databaseTools";

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: "/",
      name: "root",
      component: () => import("../views/Dashboard.vue"),
    },
    {
      path: "/movies",
      name: "movies",
      component: () => import("../views/Movies.vue"),
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: "/wishlist",
      name: "wishlist",
      component: () => import("../views/Wishlist.vue"),
      meta: {
        requiresAuth: true,
      },
    },
  ],
});

router.beforeEach(async (to, from, next) => {
  const requiresAuth = to.matched.some((record) => record.meta.requiresAuth);
  const currentUser = await getCurrentUser();
  if (currentUser && to.name === "root") next({ name: "movies" });
  else if (requiresAuth && !currentUser) {
    next({ name: "root" });
  } else {
    next();
  }
});

export default router;
