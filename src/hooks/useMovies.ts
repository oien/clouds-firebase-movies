import { onMounted, computed } from "@vue/runtime-dom";
import { FETCH_MOVIES, useStore } from "../store";

export default function useMovies() {
  const store = useStore();
  const movies = computed(() => store.state.movies);
  const loading = computed(() => store.state.loading || !store.state.hasLoaded);

  onMounted(() => store.dispatch(FETCH_MOVIES));
  return {movies, loading}

}
